package ejercicio5;

import java.io.BufferedReader;
import java.io.FileReader;

public class Adder {
    public static void main(String[] args) {
        //comprobamos que se hayan pasado argumentos al programa y se cogero el primero
        if (args.length > 0) {
            //asignamos el primer argumento a una variable y se crea un contador inicado a 0
            String fichero = args[0];
            Double total = 0.0;
            try (BufferedReader br = new BufferedReader(new FileReader(fichero))) {
                //se lee linea por linea el contenido del fichero
                String linea = br.readLine();
                while(linea != null) {
                    //sumamos el contenido de la linea al total , si no es numerico dara una excepcion y se lee la siguiente linea
                    total += Double.valueOf(linea);
                    linea = br.readLine();
                }
                System.out.println(total);
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("No se ha pasado nada");
        }
    }
}
