package ejercicio5;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ex5_Accounter {
    public static void main(String[] args) {
        //comprobamos que se han introducido argumentos al programa
        if(args.length > 0) {
            //convertimos el array de strings en lista, asignamos el comando para ejecutar el hijo como variable y se indica como varaiable el nombre del fichero
            List<String> lista = Arrays.asList(args);
            String comando = "java -jar Adder.jar";
            String fichero = "Resultado.txt";
            ProcessBuilder pb;
            Scanner scanner;
            Double totalDeTodo = 0.0;
            //separamos el comando por valores independientes en una lista
            List<String> lista2 = new ArrayList<>(Arrays.asList(comando.split(" ")));
            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fichero)));
                bw.write("Fichero - Subtotal\n");
                for (int i = 0; i < lista.size(); i++) {
                    //agregamos a la lista del comando el nombre del fichero para que el proceso hijo haga el calculo de ese fichero e iniciamos el proceso
                    lista2.add(lista.get(i));
                    pb = new ProcessBuilder(lista2);
                    Process process = pb.start();
                    //capturamos la salida del proceso hijo, se le asigna una variabl
                    scanner = new Scanner(process.getInputStream());
                    String resultado = scanner.nextLine();
                    //extraemos el valor numerico de la salida del proceso hijo,añadimos el valor del proceso hijo al total
                    Double total = Double.valueOf(resultado);
                    totalDeTodo += total;
                    //escribimos en el fichero de salida el nombre del fichero y el subtotal
                    bw.write(lista.get(i) + " - " + total + "\n");
                    //quitamos el nombre del fichero del comando
                    lista2.remove(lista.get(i));
                }
                //escribimos en el fichero de salida el total de los subtotales
                bw.write("Total - " + totalDeTodo + "\n");
                //mostramos por pantalla el total de los subtotales y el nombre del fichero generado
                System.out.println("Proceso finalizado,total: " + totalDeTodo);
                System.out.println("Fichero con resultados creado: " + fichero);
                bw.close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("introduce archivos con numero");
        }

    }
}
