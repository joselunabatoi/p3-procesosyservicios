package ejercicio4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ex4 {

    private static List<String> traducir;
    private static final String ATRIBUTO_DICCIONARIO = "-d";
    private static final String ATRIBUTO_FICHERO = "-f";
    private static String diccionario;
    private static String fichero;

    public static void main(String[] args) {
        //iniciamos la lista para guarar las traducciones y comprobamos si hay atributos
        comprobar(args);
        traducir = new ArrayList<>();

        //comando para ejecutar el hijo y lo convertimos en cadena de texto
        String comando = "java -jar Translator.jar";
        List<String> listaComando = new ArrayList<>(Arrays.asList(comando.split(" ")));
        //si la variable del diccionario contiene un valor se la añadimos al comando
        if (diccionario != null) {
            listaComando.add(diccionario);
        }

        //iniciamos el proceso hijo
        ProcessBuilder pb = new ProcessBuilder(listaComando);
        try {
            Process process = pb.start();
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
            //capturamos la salida del hijo con un scanner
            Scanner trac = new Scanner(process.getInputStream());

            //si el usuario ha pasado un fichero para traducir se utilizaran las palabras que este contenga
            if (fichero != null) {
                //se lee el fichero y se asigna la primera linea a una variable
                BufferedReader br = new BufferedReader(new FileReader(fichero));
                String original = br.readLine();
                String numero;

                //mientras que el fichero aun tenga lineas que leer le seguimos pasando palabras al hijo
                while (original != null) {
                    //pasamos la palabra al procceso mediante el buffer
                    bw.write(original);
                    bw.newLine();
                    bw.flush();
                    //leemos la salida del proceso , añadimos la traduccion al fichero y se lee la siguiente linea
                    numero = trac.nextLine();
                    añadirALista(original, numero);
                    original = br.readLine();
                }
                br.close();
            } else {
                //es lo mismo que antes pero esta vez se lee las palabras por entrada de texto estandar
                System.out.println("Si desea detener el proceso pulse \"0\"");
                Scanner sc = new Scanner(System.in);
                String original = sc.nextLine();
                String numero;
                while (!original.equals("0")) {
                    bw.write(original);
                    bw.newLine();
                    bw.flush();
                    numero = trac.nextLine();
                    System.out.println(numero);
                    añadirALista(original, numero);
                    original = sc.nextLine();
                }
                sc.close();
            }
            //se llama al metodo guardar para almacenar las traducciones en el fichero
            guardar();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    private static void añadirALista(String orig, String num) {
        //se añade a la lista la palabra original y la traducida
        traducir.add("<" + orig + "> - >> <" + num + ">\n");
    }

    private static void guardar() {
        try {
            //se inicia el fichero donde se guardan las traducciones realizadas y se guarda en el todas la traducciones
            File f = new File("Translations.txt");
            BufferedWriter bwr = new BufferedWriter(new FileWriter(f));
            for (String string : traducir) {
                bwr.write(string);
            }
            bwr.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
    private static void comprobar(String[] args) {
        //pasamos los argumentos a lista
        List<String> lis = Arrays.asList(args);
        //comprobamos si los argumentos tienen el atributo -d en caso de que si lo contenga,
        //comprobamos que el atributo tenga continuacion y se le asigna el valor al diccionario
        if (lis.contains(ATRIBUTO_DICCIONARIO)) {
            if (lis.indexOf(ATRIBUTO_DICCIONARIO) < lis.size() - 1) {
                diccionario = lis.get(lis.indexOf(ATRIBUTO_DICCIONARIO) + 1);
            } else {
                System.out.println("Falta el nombre del diccionario, se usara el interno");
            }
        } else {
            System.out.println("No se ha dado ningun diccionario, se usara el interno");
        }
        //comprobamos si los argumentos tienen el atributo -f en caso de que si lo contenga,
        //comprobamos que el atributo tenga continuacion y se le asigna el valor al fichero
        if (lis.contains(ATRIBUTO_FICHERO)) {
            if (lis.indexOf(ATRIBUTO_FICHERO) < lis.size() - 1) {
                fichero = lis.get(lis.indexOf(ATRIBUTO_FICHERO) + 1);
            } else {
                System.out.println("Falta nombre del fichero, introduzca las palabras a mano");
            }
        } else {
            System.out.println("No se ha dado ningun fichero, introduzca las palabras a mano");
        }

    }




}
