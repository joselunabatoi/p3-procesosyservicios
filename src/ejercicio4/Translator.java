package ejercicio4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Scanner;

public class Translator {
    private static HashMap<String, String> hashMap;
    //archivo por defecto para las traducciones
    private static String archivo = "diccionario.txt";

    public static void main(String[] args) {
        hashMap = new HashMap<>();
        //pasamos los argumentos a un metodo para comprobar si se ha introducido un diccionario externo y se lee la entrada de texto
        listaNueva(args);
        Scanner sc = new Scanner(System.in);
        String linea;
        linea = sc.nextLine();
        while (!linea.toLowerCase().equals("0")) {
            //si no se conoce esa palabra mostrara desconocido y se leera la siguiente linea
            if (hashMap.containsKey(linea)) {
                System.out.println(hashMap.get(linea));
            } else {
                System.out.println("desconocido");
            }
            linea = sc.nextLine();
        }
        sc.close();
    }

    private static void listaNueva(String[] lista) {
        //se van pasando los argumentos siempre que no esten vacios y se le asigna el valor al diccionario
        if (lista.length > 0) {
            archivo = lista[0];
        }
        try {
            //se lee el fichero por lineas y se pasa cada una a un metodo para separarla por palabras para posteriormente traducirlas
            File f = new File(archivo);
            BufferedReader br = new BufferedReader(new FileReader(f));
            String linea = br.readLine();
            while(linea != null) {
                separarPalabras(linea);
                linea = br.readLine();
            }
            br.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void separarPalabras(String linea) {
        //se divide por palabras y se va guardando en el hasmap
        String[] conjunto = linea.split(";");
        hashMap.put(conjunto[0], conjunto[1]);
    }
}
