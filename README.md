# P3-ProcesosYServicios

Ejercicio 4

Para este ejercicio puedes crear tu propio diccionario externo
Debera escribir en un fichero todas las palabras que quieras con su traduccion debes de escribirla siguiendo este orden:
"original";"traduccion"

Las palabras traducidas van a guardarse en un fichero aparte
Se puede ejecutar de 3 formas:
1-sin argumentos
java -jar Ex4.jar

2-pasando el diccionario
java -jar Ex4.jar -d "diccionario"

3-pasando un fichero con las palabras
java -jar Ex4.jar -f "fichero"

Ejercicio 5
En este programa sumara los numero que un fichero contenga. Para eso se debera pasar almenos 1 fichero con un numero distinto en cada linea

Ejecucion:
java -jar Ex5_Accounter.jar "fichero"
